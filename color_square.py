import re
import os
import datetime
import colorsys

from matplotlib import pyplot
from PIL import Image, ImageOps
from django.conf import settings


class ColorSquare:
    def __init__(self, upload_to='to/jest/test', size_in_pixels=100, file_format='png',
                 upload_base=settings.MEDIA_ROOT, *args, **kwargs):

        try:
            self.data = kwargs['data']
        except:
            self.data = [["#78ff00", 50], ["#ff00ff", 25], ["#452222", 25]]

        self.upload_to = os.path.join(upload_base, upload_to)
        self.size_in_pixels = size_in_pixels
        self.file_format = file_format

    @classmethod
    def generate(cls, filename, data, size_in_pixels):

        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        #filename = os.path.join(self.upload_to, filename)
        file_format = 'png'

        colors = []
        for x in range(len(data)):
            if data[x][0][:3] == 'hsv':
                matches = re.findall(r'\d+', data[x][0])
                rgb = colorsys.hsv_to_rgb(int(matches[0]) / 360, int(matches[1]) / 100, int(matches[2]) / 100)
                colors.append((rgb[0], rgb[1], rgb[2]))
            else:
                colors.append(data[x][0])

        percents = []
        for x in range(len(data)):
            percents.append(data[x][1])

        fig = pyplot.figure(num=1, figsize=(2, 2))
        pyplot.axes(aspect=1)

        pyplot.pie(percents, colors=colors, startangle=315)

        pyplot.axis('equal')

        if not os.path.exists(filename):
            open(filename, 'a').close()

        pyplot.savefig(filename, format=file_format, bbox_inches='tight', transparent=True, pad_inches=0.0)
        img = Image.open(filename)

        # remove margins from plot image
        img = img.crop((10, 10, img.width - 10, img.height - 10))
        img = img.crop((22, 22, 120, 120))
        img = ImageOps.fit(img, (size_in_pixels, size_in_pixels))
        img.save(filename)
