$(document).ready(function () {

    console.log('load!');

    $('.colorfield_field').spectrum({preferredFormat:'hex'});

    $('.addnew').on('click', function (e) {
        e.preventDefault();
        ele = $('<div>');
        ele.css({'padding-bottom': '10px', 'display': 'block'});
        ele.attr('class', 'color_div_'+$('.colors').children().length)
        delete_button = $('<span>')
        delete_button.attr('class', 'delete_button')
        delete_button.attr('data-row', $('.colors').children().length)
        delete_button.attr('style', 'margin-left: 3px')
        image = $('<img>')
        image.attr('src', '/static/colorfield/img/remove.png')
        delete_button.append(image)
        delete_button.on('click', ondelete)

        name_ele = $('<input>');
        name_ele.attr('type', 'text');
        name_ele.attr('name', 'name_'+$('.colors').children().length);
        name_ele.attr('style', 'margin-right: 4px;')
        input = $('<input>');
        input.attr('type', 'text');
        input.attr('name', 'color_'+$('.colors').children().length);
        input.attr('class', 'colorfield_field');
        input.attr('requred', 'requred');


        radio = $('<input>')
        radio.attr('type', 'radio');
        radio.attr('class', 'color-radio');
        radio.attr('data-row', $('.colors').children().length)
        radio.attr('name', 'color-radio')
        radio.attr('style', 'margin: 0 3px')
        percent = $('<input>');
        percent.attr('type', 'number');
        percent.attr('min', '1');
        percent.attr('max', '100');
        percent.attr('name', 'percent_'+$('.colors').children().length)
        percent.attr('placeholder', 'Value in %...')

        var sum = 0;
        var i = 0;
        while(true){
            console.log('tru')

            if($('input[name="percent_'+i+'"').length == 0){
                break
            }
            else {
                sum += parseInt($('input[name="percent_'+i+'"').val())
            }
            i++;
        }
        if(sum < 100){
            sum = 100 - sum;
            percent.val(sum);
        }


        ele.append(name_ele)
        ele.append(input);
        ele.append(radio)
        ele.append(percent);
        ele.append(delete_button)

        $('.colors').append(ele);
        $('.colorfield_field').spectrum()
        $('.colorfield_field').last().spectrum({'color': '#000', preferredFormat:'hex'})
    });
    function ondelete(e) {
        e.preventDefault()
        row = $(this).attr('data-row');
        $('.color_div_'+row).remove();
        temp = parseInt(row) + 1;

        while(true){
            if($('input[name=name_'+temp+']').length == 0){
                break
            }
            $('div[class=color_div_'+temp+']').attr('class', 'color_div_'+(temp-1));
            $('input[name=name_'+temp+']').attr('name', 'name_'+(temp-1));
            $('input[name=color_'+temp+']').attr('name', 'color_'+(temp-1));
            $('input[name=percent_'+temp+']').attr('name', 'percent_'+(temp-1));
            $('span[data-row='+temp+']').attr('data-row', (temp-1));
            $('input.color-radio[data-row='+temp+']').attr('data-row', (temp-1));
            temp++;
        }
        console.log(row)
    }
    $('.delete_button').on('click', ondelete)
    function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            console.log(e)
            $('#blah').attr('src', e.target.result);
            $('#blah').css({opacity: 1})
            $('.preview').css({opacity: 1})
        }
        reader.readAsDataURL(input.files[0]);
    }
}

    $("#imgInp").change(function(){
        console.log('change')
     readURL(this);
    });
});
