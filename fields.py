#!/usr/bin/env python
import os
import json
import uuid
import random

from django import forms
from django.conf import settings
from django.db import models
from django.template.loader import render_to_string

from .color_square import ColorSquare


class BestColorWidget(forms.Widget):
    upload_to = None
    size_in_pixels = None
    class Media:
        css = {
            'all': ['colorfield/css/spectrum.css']
        }
        js = ['https://code.jquery.com/jquery-3.1.1.min.js', 'colorfield/js/spectrum.js', 'colorfield/js/addnew.js', 'colorfield/js/picker.js']

    def __init__(self, upload_to=None, size_in_pixels=None, *args, **kwargs):
        self.upload_to = upload_to
        self.size_in_pixels = size_in_pixels
        self.object = ColorSquare(size_in_pixels=self.size_in_pixels, upload_to=self.upload_to)
        super(BestColorWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None):
        ctx = {}
        try:
            value = json.loads(value.data)
            rows = range(len(value) - 1)
            filename = value[-1]
        except Exception as e:
            rows = range(0)
            filename = None
        ctx['value'] = value
        ctx['rows'] = rows
        ctx['filename'] = filename
        ctx['random'] = random.randint(0,1000)



        if filename != None:
            path_to_generated_image = os.path.join(settings.MEDIA_URL, str(filename))
            ctx['path_to_generated_image'] = path_to_generated_image

        return render_to_string('colorfield/single.html', ctx)
    last_token = [0,0]
    def value_from_datadict(self, data, files, name):
        print('VALUE FROM DATADICT')
        print(str(locals()))
        filename = data['filename']

        if filename == 'None' and self.last_token[0] != data['csrfmiddlewaretoken']:
            print('generate filename')
            filename = os.path.join(self.upload_to,  str(uuid.uuid4())+'.png')
            self.last_token[0] = data['csrfmiddlewaretoken']
            self.last_token[1] = filename

        my_data = []
        i = 0

        while True:
            try:
                if data['color_{}'.format(i)] == '':
                    data['color_{}'.format(i)] = '#000000'
                if data['percent_{}'.format(i)] == '':
                    data['percent_{}'.format(i)] = 0

                my_data.append([data['color_{}'.format(i)], data['percent_{}'.format(i)],
                                data['name_{}'.format(i)]])
                i += 1
            except:
                break
        my_data.append(filename)
        if len(my_data) <= 1: #0 colors, only filename
            try:
                os.remove(os.path.join(settings.MEDIA_ROOT, filename))
            except:
                pass
            my_data = []
        else:
            if self.last_token[0] == data['csrfmiddlewaretoken']:
                ColorSquare.generate(filename=os.path.join(settings.MEDIA_ROOT, self.last_token[1]), size_in_pixels=self.size_in_pixels, data=my_data[:-1])
            elif filename != 'None':
                ColorSquare.generate(filename=os.path.join(settings.MEDIA_ROOT, filename),
                                     size_in_pixels=self.size_in_pixels, data=my_data[:-1])
        return json.dumps(my_data)


class ColorField(models.Field):
    upload_to=None
    size_in_pixels = None
    def __init__(self, upload_to=None, size_in_pixels=50, *args, **kwargs):
        self.upload_to = upload_to
        self.size_in_pixels = size_in_pixels
        kwargs['max_length'] = 500
        super(ColorField, self).__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super(ColorField, self).deconstruct()
        del kwargs["max_length"]
        return name, path, args, kwargs

    def get_prep_value(self, value):
        if value is None:
            return None
        return json.loads(value)

    def to_python(self, value):
        return json.dumps(value)

    def from_db_value(self, *args, **kwargs):
        self.data = args[0]
        return self

    def formfield(self, **kwargs):

        kwargs['widget'] = BestColorWidget(upload_to=self.upload_to, size_in_pixels=self.size_in_pixels)
        return super(ColorField, self).formfield(**kwargs)

    def db_type(self, connection):
        return 'TEXT'

    @property
    def path(self):
        return json.loads(self.data)[-1]

    @property
    def url(self):
        return os.path.join(settings.MEDIA_URL, self.path)
