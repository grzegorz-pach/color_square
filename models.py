from django.db import models

from .fields import ColorField

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=200)
    path_to_image = models.ImageField()
    colorsquare = ColorField(size_in_pixels=100, upload_to='test/to', null=True, blank=True)


    def __str__(self):
        return str(self.name)
